<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\View;
use App\Utilities\CommonFunction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $request = (new Request)::createFromGlobals();
      View::share('get',$request->all());
      // View::share('stores',CommonFunction::loadStores());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      $data = [];
      return view('home',$data);
    }

    public function getSearchResults(Request $request) {
      $data = [];
      return view('search-results',$data);
    }
}
