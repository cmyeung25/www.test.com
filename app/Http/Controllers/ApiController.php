<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use App\Utilities\CommonFunction;
use App\Utilities\StoreProduct;
use Illuminate\Support\Collection;
use NumberFormatter;
use Money\Currencies\ISOCurrencies;
use Money\Parser\IntlLocalizedDecimalParser;
use Money\Parser\IntlMoneyParser;
use Money\Currency;

use Config;
use Storage;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function getLookup(Request $request) {
      $stores = CommonFunction::loadStores();

      $data = [];
      $site = !empty($request->get('site')) ? $request->get('site') : "amazon";
      $page = !empty($request->get('page')) ? $request->get('page') : 1;
      $keyword = urlencode($request->get('keyword'));

      $store = $stores->where('key',$site)->first();

      $siteResults = [];
      if(!empty($store)) {
        $url = $store->fetchUrl;
        // foreach ($sites as $site => $url) {
        $url = $url . "search?sq={$keyword}&p={$page}";
        $dom = new Dom;
        // $dom->loadFromUrl($url);
        $dom->loadFromFile(base_path("resources/dummy/{$store->dummyFile}.html"));
        $searchResults = $dom->find('.search_results');
        if($searchResults->count() > 0) {
          $rows = $searchResults->find('.row');

          $storeProducts = new Collection();

          foreach ($rows as $row) {
            if($row->getTag()->getAttribute('class')['value'] == "row") {
              $storeProduct = new StoreProduct();
              // "currency",
              $storeProduct->currency = $store->currency;

              // "img",
              $storeProduct->img = $row->find('[x-camel-place="Search - Thumb"] img')[0]->getTag()->getAttribute('src')['value'];
              if(strpos($storeProduct->img,'thumbna') > 0) {
                $storeProduct->img = null;
              }

              // "title",
              $storeProduct->title = $row->find('[x-camel-place="Search - Title"]')->text();

              $categories_raw = $row->find('.breadcrumbs li a');
              $categories = new Collection();
              foreach ($categories_raw as $key => $category_raw) {
                $categories->push($category_raw->text());
              }

              // "categories",
              $storeProduct->categories = $categories;


              // "price","priceStr"
              $price0 = $this->getAmazonPrice($row, $store, '.price0 .green');
              $price1 = $this->getAmazonPrice($row, $store, '.price1 .green');
              $price2 = $this->getAmazonPrice($row, $store, '.price2 .green');

              $storeProduct->price0 = $price0['price'];
              $storeProduct->priceStr0 = $price0['priceStr'];
              $storeProduct->price = $storeProduct->price0;
              $storeProduct->priceStr = $storeProduct->priceStr0;

              $storeProduct->price1 = $price1['price'];
              $storeProduct->priceStr1 = $price1['priceStr'];
              if(empty($storeProduct->price) || (!empty($storeProduct->price1) && $storeProduct->price1 < $storeProduct->price)) {
                $storeProduct->price = $storeProduct->price1;
                $storeProduct->priceStr = $storeProduct->priceStr1;
              }

              $storeProduct->price2 = $price2['price'];
              $storeProduct->priceStr2 = $price2['priceStr'];
              if(empty($storeProduct->price) || (!empty($storeProduct->price2) && $storeProduct->price2 < $storeProduct->price)) {
                $storeProduct->price = $storeProduct->price2;
                $storeProduct->priceStr = $storeProduct->priceStr2;
              }

              $itemPropTitles = $row->find('strong');
              foreach ($itemPropTitles as $itemPropTitle) {
                if($itemPropTitle->text() == "EAN:") {
                  // "ean",
                  $storeProduct->ean = trim($itemPropTitle->getParent()->text());
                }

                if($itemPropTitle->text() == "Model:") {
                  // "ean",
                  $storeProduct->model = trim($itemPropTitle->getParent()->text());
                }
                // "model",
              }


              // "asin",
              $storeProduct->asin = $row->find('[x-camel-place="Search - Title"]')->getTag()->getAttribute('x-camel-asin')['value'];

              // "store",
              $storeProduct->store = $store->key;

              // "url",

              $storeProducts->push($storeProduct);
            }
          }
          $siteResults = $storeProducts;
        }
        // }

        $data['siteResults'] = $siteResults;
        return response()->json($data);
      }
    }

    //camelcamelcamel
    protected function getAmazonPrice($row, $store, $selector) {
      $priceRaw = $row->find($selector);
      $priceStr = null;
      if($priceRaw->count() > 0) {
        $priceStr = $priceRaw->text();
      }

      $price = null;
      if(!empty($priceStr)) {
        $currencies = new ISOCurrencies();
        switch ($store->currency) {
          case 'EUR':
            $numberFormatter = new \NumberFormatter($store->currency_locale, \NumberFormatter::DECIMAL);
            $moneyParser = new IntlLocalizedDecimalParser($numberFormatter, $currencies);
            break;
          case 'GBP':
            $numberFormatter = new \NumberFormatter($store->currency_locale, \NumberFormatter::CURRENCY);
            $moneyParser = new IntlMoneyParser($numberFormatter, $currencies);
            break;
          default:
            $numberFormatter = new \NumberFormatter($store->currency_locale, \NumberFormatter::CURRENCY);
            $moneyParser = new IntlLocalizedDecimalParser($numberFormatter, $currencies);
            break;
        }
        $money = $moneyParser->parse($priceStr, new Currency($store->currency));
        $price = $money->getAmount();
      }
      return [
        "price" => $price,
        "priceStr" => $priceStr,
      ];
    }

    public function getExchangeRate(Request $request) {
      $json = json_decode(Storage::disk('local')->get('rate.json'));
      return response()->json($json);
    }
}
