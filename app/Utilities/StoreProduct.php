<?php

namespace App\Utilities;
use Illuminate\Database\Eloquent\Model;

class StoreProduct extends Model
{
  protected $fillable = [
    "model",
    "asin",
    "categories",
    "title",
    "ean",
    "img",
    "currency",
    "price",
    "priceStr",
    "price0",
    "priceStr0",
    "price1",
    "priceStr1",
    "price2",
    "priceStr2",
    "store",
    "url",
  ];
}
