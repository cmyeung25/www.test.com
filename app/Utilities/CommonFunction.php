<?php
namespace App\Utilities;
use App\Utilities\Store;
use App\Utilities\StoreProduct;
use Illuminate\Support\Collection;
use Config;
use Storage;

class CommonFunction{
  public static function loadStores() {
    $storesConfig = config('stores');
    $stores = new Collection();
    foreach ($storesConfig as $key => $config) {
      $store = new Store();
      $store->key = $key;
      $store->fill($config);
      $stores->push($store);
    }
    return $stores;
  }
}
