import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import Header from './components/Header'
import SearchResults from './components/SearchResults'
import SearchBar from './components/SearchBar'
import store from "./store/index";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

render(
  <Provider store={store}>
    <Router>
      <Header />
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <Switch>
              <Route exact path="/">
                <SearchBar/>
              </Route>
              <Route path="/about">
                About
              </Route>
              <Route path="/search-results">
                <SearchBar/>
                <SearchResults/>
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
)
