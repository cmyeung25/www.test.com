import {
  ADD_ARTICLE,
  ADD_EXCHANGE_RATE,
  CHANGE_CURRENCY,
  CHANGE_KEYWORD,
  ADD_SEARCH_RESULT_PRODUCT,
} from "../constants/action-types";

const getData = JSON.parse(window.getData);
const initialState = {
  currentCurrency: "HKD",
  exchangeRates: {},
  availableCurrencies : ["HKD","CAD","GBP","JPY","THB","EUR","MYR","CNY","USD","SGD","AUD","KRW"],
  stores: {
    'amazon': {"selected":true},
    'amazon_au': {"selected":true},
    'amazon_ca': {"selected":true},
    // 'amazon_jp': {"selected":true},
    'amazon_uk': {"selected":true},
  },
  keyword:getData.keyword,
  searchResult: {
    done: false,
    hasMore: false,
    elements: [],
    page: 0,
    loadingMore:false,
  },
};

function rootReducer(state = initialState, action) {
  if (action.type === ADD_EXCHANGE_RATE) {
    state = {...state,exchangeRates:action.payload};
  }
  if (action.type === CHANGE_CURRENCY) {
    state = {...state,currentCurrency:action.payload};
  }
  if (action.type === CHANGE_KEYWORD) {
    state = {...state,keyword:action.payload};
  }
  if (action.type === ADD_SEARCH_RESULT_PRODUCT) {
    state = {...state,searchResult:action.payload};
  }


  return state;
};
export default rootReducer;
