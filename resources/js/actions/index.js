import {
  ADD_ARTICLE,
  ADD_EXCHANGE_RATE,
  CHANGE_CURRENCY,
  CHANGE_KEYWORD,
  ADD_SEARCH_RESULT_PRODUCT,
} from "../constants/action-types";

export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload }
};

export function addExchangeRate(payload) {
  return { type: ADD_EXCHANGE_RATE, payload }
};

export function changeCurrency(payload) {
  return { type: CHANGE_CURRENCY, payload }
};

export function changeKeyword(payload) {
  return { type: CHANGE_KEYWORD, payload }
};
export function addSearchResultProduct(payload) {
  return { type: ADD_SEARCH_RESULT_PRODUCT, payload }
};
