import React from 'react';
import ReactDOM from 'react-dom';
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import ReactLoading from "react-loading";
import MasonryInfiniteScroller from 'react-masonry-infinite';
import * as loading from "../../json/shopping-bag.json";
import store from '../store/index';
import Dotdotdot from 'react-dotdotdot'
import NumberFormat from 'react-number-format';
import LocalizedStrings from 'react-localization';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: loading.default,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
}

const masonrySizes = [
  { columns: 1, gutter: 20 },
  { mq: '768px', columns: 2, gutter: 20 },
  { mq: '1024px', columns: 4, gutter: 20 },
  { mq: '1440px', columns: 4, gutter: 20 },
]

let strings = new LocalizedStrings({
 en:{
   "store_amazon":"Amazon US ",
   "store_amazon_au":"Amazon Australia",
   "store_amazon_ca":"Amazon Canada",
   "store_amazon_cn":"Amazon China",
   "store_amazon_fr":"Amazon France",
   "store_amazon_de":"Amazon Germany",
   "store_amazon_it":"Amazon Italy",
   "store_amazon_jp":"Amazon Japay",
   "store_amazon_es":"Amazon Spain",
   "store_amazon_uk":"Amazon United Kingdom",
 },
 zh: {
   "store_amazon":"Amazon US ",
   "store_amazon_au":"Amazon Australia",
   "store_amazon_ca":"Amazon Canada",
   "store_amazon_cn":"Amazon China",
   "store_amazon_fr":"Amazon France",
   "store_amazon_de":"Amazon Germany",
   "store_amazon_it":"Amazon Italy",
   "store_amazon_jp":"Amazon Japay",
   "store_amazon_es":"Amazon Spain",
   "store_amazon_uk":"Amazon United Kingdom",
 }
});

class SearchResults extends React.Component {
    store = this.getCurrentStateFromStore();

    getCurrentStateFromStore() {
      return {
        currentCurrency: store.getState().currentCurrency,
        availableCurrencies: store.getState().availableCurrencies,
        exchangeRates: store.getState().exchangeRates,
        stores: store.getState().stores
      }
    }

    updateStateFromStore = () => {
      const currentState = this.getCurrentStateFromStore();

      if (this.state !== currentState) {
        this.setState(Object.assign({}, this.state, currentState));
      }
    }

    constructor(props) {
      super(props);
      this.state = {
        done: false,
        hasMore: false,
        elements: [],
        page: 0,
      };
    }

    componentDidMount () {
      this.unsubscribeStore = store.subscribe(this.updateStateFromStore);
      this.fetchNewContent();
    }

    fetchNewContent = () => {

      Promise.all(this.getProductListFromStores())
      .then((resultLists) => {
        let items = [];
        for (var resultList of resultLists) {
          items = items.concat(resultList.siteResults);
        }
        for (var item of items) {
          item.unifiedAmount = this.covertedPrice(item.price, item.currency, "USD")
        }

        let elements = this.state.elements.concat(items)
        elements.sort((a,b) => {
          if (a.unifiedAmount > b.unifiedAmount) {
            return 1;
          }
          if (b.unifiedAmount > a.unifiedAmount) {
            return -1;
          }
          return 0;
        })

        this.setState(state => ( {
           done: true,
           hasMore: true,
           elements: elements
         }));
      })
    }

    componentWillUnmount() {
      this.unsubscribeStore();
    }

    getProductListFromStores = () => {
      let calls = [];

      for (var i in this.store.stores) {
        if(this.store.stores[i].selected) {
          calls.push(fetch("/api/lookup?keyword=gopro&site=" + i).then(res => res.json()));
        }
      }
      return calls;
    }

    loadMore = () => {
      // this.fetchNewContent();
    }

    covertedPrice = (oriPrice, oriCurrency, targetCurrency) => {
      let price = null;
      if(typeof this.state.exchangeRates != undefined) {
        price = oriPrice/this.state.exchangeRates["rates"][targetCurrency][oriCurrency]
      }
      return Math.round(price/100)
    }


    render() {
      const { currentCurrency,exchangeRates } = this.state;
      return (
        <div className="container search-results">
        {!this.state.done ? (
          // <ReactLoading type={"bars"} color={"black"}/>
          <FadeIn>
            <div className="d-flex justify-content-center align-items-center">
              <Lottie options={defaultOptions} height={300} width={300} />
            </div>
          </FadeIn>
        ) : (
          <div className="row">
            <div className="col-md-12">
              <MasonryInfiniteScroller
                hasMore={this.state.hasMore}
                loadMore={this.loadMore}
                className="masonry"
                sizes={masonrySizes}
                >
                  {
                    this.state.elements.map((item, i) => (
                      <div className="result-card" key={"result-card-" + i}>
                        <div className="result-card-shadow">
                          <a href="#">Shop Now</a>
                        </div>
                        <div className="result-img" style={{backgroundImage:`url(${item.img})` }}></div>
                        <div className="result-card-content">
                          <div className="result-card-title">
                            <Dotdotdot clamp={2}>
                              {item.title}
                            </Dotdotdot>
                          </div>
                          <div className="result-model-number">
                            Model: {item.model}
                          </div>
                          <div className="result-card-price">
                            <span className="result-card-self-currency">{currentCurrency}</span>
                            <span className="result-card-self-amount"><NumberFormat thousandSeparator={true}  value={this.covertedPrice(item.price, item.currency, currentCurrency)} displayType={'text'}/></span>
                            <span className="result-card-ori-currency">({item.currency}</span>
                            <span className="result-card-ori-amount"><NumberFormat thousandSeparator={true}  value={item.price/100} displayType={'text'}/>)</span>
                          </div>
                          <div className="result-exchange-rate">
                            1 {currentCurrency} = {Math.round(exchangeRates["rates"][currentCurrency][item.currency] * 10000) / 10000} {item.currency}
                          </div>
                          <div className="result-card-category">
                            <ul className="result-card-tag">
                              {
                                item.categories.map((category, j) => (
                                  (
                                    <li className="" key={"result-card-category-" + j}><a href="#" className="badge badge-pill badge-warning">{category}</a></li>
                                  )
                                ))
                              }
                            </ul>
                          </div>
                          <div className="result-card-store">Source: {strings['store_' + item.store]}</div>
                        </div>
                      </div>
                    ))
                  }
              </MasonryInfiniteScroller>
            </div>
          </div>
        )}
        </div>

      );
    }
}

export default SearchResults;

if (document.getElementById('search-results')) {
    ReactDOM.render(<SearchResults />, document.getElementById('search-results'));
}
