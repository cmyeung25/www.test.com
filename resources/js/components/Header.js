import React from 'react';
import ReactDOM from 'react-dom';
import store from '../store/index';
import { connect } from "react-redux";
import { Dropdown } from "react-bootstrap";
import { addExchangeRate, changeCurrency  } from "../actions/index";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    fetch("/api/exchange-rate")
    .then(res => res.json())
    .then(
      (result) => {
        this.props.addExchangeRate(result);
      }
    )
  }

  render() {
    return (
      <nav className="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div className="container">
          <a href="#" className="navbar-brand">Brand</a>
        </div>
        <Dropdown>
          <Dropdown.Toggle className="nav-curreny-selector">
            {this.props.currentCurrency}
          </Dropdown.Toggle>
          <Dropdown.Menu>
          {
            this.props.availableCurrencies.map((currency, i) => (
              (
                <Dropdown.Item key={"currency-dropdown-" + i} onSelect={this.props.changeCurrency} eventKey={currency}>{currency}</Dropdown.Item>
              )
            ))
          }
          </Dropdown.Menu>
        </Dropdown>
      </nav>
    );
  }
}
const mapDispatchToProps = function (dispatch) {
  return {
    addExchangeRate: payload => dispatch(addExchangeRate(payload)),
    changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  console.log(state);
  return {
    currentCurrency: state.currentCurrency,
    availableCurrencies: state.availableCurrencies
  };
};

export default connect(mapStateToProps, mapDispatchToProps )(Header);
window.store = store
