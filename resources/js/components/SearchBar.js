import React from 'react';
import ReactDOM from 'react-dom';
import store from '../store/index';
import { connect } from "react-redux";
import { changeKeyword } from "../actions/index";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
  }
  setKeyword = (e) => {
    this.props.changeKeyword(e)
  }

  render() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="search-bar">
                    <form action="/search-results" method="get">
                      <div className="form-group">
                        <div className="input-group mb-3">
                          <input type="text" name="keyword" className="form-control" placeholder="Input keywords" value={this.props.keyword} onChange={e => this.setKeyword(e.target.value)}/>
                          <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="sumbit"><i className="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

const mapDispatchToProps = function (dispatch) {
  return {
    changeKeyword: payload => dispatch(changeKeyword(payload)),
    // changeCurrency: payload => dispatch(changeCurrency(payload)),
  };
}

const mapStateToProps = function(state) {
  return {
    keyword: state.keyword,
    // availableCurrencies: state.availableCurrencies
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
